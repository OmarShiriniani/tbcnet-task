import React, { useEffect, useState } from 'react';
import { Form } from 'react-bootstrap';

const PeriodOptions = [
  { label: '1 საათი', value: '1h' },
  { label: '2 შსაათი', value: '2h' },
  { label: '3 საათო', value: '3h' },
  { label: '1 დღე', value: '1d' },
  { label: '2 დღე', value: '2d' },
  { label: '3 დრე', value: '3d' },
  { label: '1 კვირა', value: '1w' },
  { label: '2 კვირა', value: '2w' },
  { label: '3 კვირა', value: '3w' },
];

const filterOptions = [
  { label: 'თარიღი კლებადი', value: 1 },
  { label: 'თარიღი ზრდადი', value: 2 },
  { label: 'ფასი კლებადი', value: 3 },
  { label: 'ფასი ზრდადი', value: 4 },
  { label: 'გარბენი კლებადი', value: 5 },
  { label: 'გარბენი ზრდადი', value: 6 },
];

function ProductList({ products, onChange }) {
  const [productCount, setProductCount] = useState(0);

  useEffect(() => {
    setProductCount(products.length);
  }, [products]);

  return (
    <div className='prod-list'>
      <div className='product-filter mb-2 '>
        <p className='mb-0 mt-2'>{productCount} განცხადება</p>

        <Form className='form d-flex'>
          <Form.Group className='mx-3'>
            <Form.Select name='Period' onChange={onChange}>
              {PeriodOptions.map((option, key) => (
                <option key={key} value={option.value}>
                  {option.label}
                </option>
              ))}
            </Form.Select>
          </Form.Group>

          <Form.Group className='mb-2'>
            <Form.Select name='SortOrder' onChange={onChange}>
              {filterOptions.map((option, key) => (
                <option key={key} value={option.value}>
                  {option.label}
                </option>
              ))}
            </Form.Select>
          </Form.Group>
        </Form>
      </div>
      {products.map((e, k) => (
        <CardList key={k} data={e} />
      ))}
    </div>
  );
}

const CardList = ({ key, data }) => {
  return (
    <div className='card-list' key={key}>
      <div className='m-actions'>
        <div className='d-flex'>
          <p>589 ნახვა</p> * <p>2 დღის წინ</p>
        </div>
        <div>
          <img src='/icons/note.svg' width={16} alt='note' />
          <img
            src='/icons/shedareba.svg'
            width={16}
            className='mx-2'
            alt='icon'
          />
        </div>
      </div>

      <div className='card-h show-m mt-2'>
        <p className='title mb-0'>LAND ROVER Range Rover Evoque</p>
        <span className='yare'>2013 წ</span>
      </div>

      <div className='m-amount'>
        <div className='d-flex'>
          <span>76,500</span>
          <img src='/icons/valute.svg' alt='valute' />
        </div>

        <div className='m-card-lside'>
          <img src='/icons/done-path.svg' alt='done' />
          განბაჟებული
        </div>
      </div>

      <div className='card-img'>
        <img
          src='https://static.my.ge/myauto/photos/0/2/5/1/8/large/66815209_8.jpg?v=3'
          alt='img'
          className='w-100'
        />
      </div>

      <div className='d-flex justify-content-between w-100'>
        <div className='card-desc'>
          <div className='card-h hide-m'>
            <p className='title'>LAND ROVER Range Rover Evoque</p>
            <span className='yare'>2013 წ</span>
          </div>

          <div className='row'>
            <div className='col-8 desc-item'>
              <img src='/icons/motor.svg' width={16} alt='motor' />{' '}
              <span>1.8 დატ. ჰიბრიდი</span>
            </div>
            <div className='col-4 desc-item'>
              <img src='/icons/speed.svg' width={16} alt='motor' />
              <span>ავტომატიკა</span>
            </div>

            <div className='col-8 desc-item'>
              <img src='/icons/33660.svg' width={16} alt='motor' />
              <span> 200 000 კმ</span>
            </div>
            <div className='col-4 desc-item'>
              <img src='/icons/sache.svg' width={16} alt='motor' />
              <span>მარჯვენა</span>
            </div>

            <div className='col-8 desc-item show-m'>
              <img src='/icons/33660.svg' width={16} alt='motor' />
              <span> ავტომატიკა</span>
            </div>
            <div className='col-4 desc-item show-m align-items-center'>
              <img
                src='/icons/geo-flag.svg'
                className='geo-flag'
                width={16}
                alt='motor'
              />
              <span className='mx-1'>თბილისი</span>
            </div>
          </div>

          <div className='views'>
            <p>589 ნახვა</p> * <p>2 დღის წინ</p>
          </div>
        </div>

        <div className='card-lside'>
          <div className='d-flex'>
            <p className='success mb-0 mx-3'>განბაჟებული</p>
            <img width={16} src='/icons/flag-geo.svg' alt='geo' />
            <p className='mb-0 mx-2'>თბილისი</p>
          </div>

          <div className='amount'>
            <span>76,500</span>
            <img src='/icons/valute.svg' alt='valute' />
          </div>

          <div className='actions'>
            <img src='/icons/note.svg' width={16} alt='note' />
            <img
              src='/icons/shedareba.svg'
              width={16}
              className='mx-2'
              alt='shedareba'
            />
            <img src='/icons/favorite.svg' width={16} alt='favorite' />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductList;
