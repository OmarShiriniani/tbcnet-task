import React from 'react';

function Breadcrumb() {
  return (
    <div className='breadcrumb'>
      <span>მთავარი</span>
      <img src='/icons/arow.svg' alt='' />
      <span>ძიება</span>
      <img src='/icons/arow.svg' alt='' />
      <span>იყიდება</span>
    </div>
  );
}

export default Breadcrumb;
