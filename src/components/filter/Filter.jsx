import React from 'react';
import { Form } from 'react-bootstrap';
function Filter({ mans, cats, filterData, onChange }) {

  const onSubmit = () => {
    filterData();
  };

 
  return (
    <div className='filter'>
      {/* Tabs */}
      <div className='tabs'>
        <div className='tab-item active'>
          <img src='/icons/car.svg' alt='car' />
        </div>
        <div className='tab-item '>
          <img src='/icons/tractor.svg' alt='tractor' />
        </div>
        <div className='tab-item '>
          <img src='/icons/moto.svg' alt='moto' />
        </div>
      </div>
      {/* Selects */}
      <div className='form mt-3'>
        <Form autoComplete='off'>
          <div className='px-4'>
            <Form.Group className='mb-2'>
              <Form.Label>გარიგების ტიპი</Form.Label>
              <Form.Select name='ForRent' onChange={onChange}>
                <option value='1'>იყიდება</option>
                <option value='2'>ქირავდება</option>
              </Form.Select>
            </Form.Group>
            <Form.Group className='mb-2'>
              <Form.Label>მწარმოებელი</Form.Label>
              <Form.Select name='Mans' onChange={onChange}>
                {mans.map((e, k) => (
                  <option key={k} value={e.man_id}>
                    {e.man_name}
                  </option>
                ))}
              </Form.Select>
            </Form.Group>
            <Form.Group className='mb-2'>
              <Form.Label>კატეგორია</Form.Label>
              <Form.Select name='Cats' onChange={onChange}>
                {cats.map((e, k) => (
                  <option key={k} value={e.category_id}>
                    {e.title}
                  </option>
                ))}
              </Form.Select>
            </Form.Group>
          </div>
          <hr />
          <div className='px-4'>
            <Form.Label>ფასი</Form.Label>
            <div className='d-flex'>
              <Form.Group>
                <Form.Control type='text' placeholder='დან' />
              </Form.Group>
              <img src='/icons/dash.svg' className='mx-2' alt='dash' />
              <Form.Group>
                <Form.Control type='text' placeholder='მდე' />
              </Form.Group>
            </div>
          </div>
          <div className='filter-footer p-4 mt-5'>
            <button
              type='button'
              onClick={onSubmit}
              className='btn primary-orange w-100'
            >
              ძებნა 197,963
            </button>
          </div>
        </Form>
      </div>
    </div>
  );
}

export default Filter;
