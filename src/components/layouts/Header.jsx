import React from 'react';

function Header() {
  return (
    <header className='header'>
      <div className='container'>
        <img src='/icons/logo.png' className='logo' alt='logo' />
      </div>
    </header>
  );
}

export default Header;
