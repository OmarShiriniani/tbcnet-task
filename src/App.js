import Layout from './components/layouts/Layout';
import MainPage from './pages/MainPage';
import 'bootstrap/dist/css/bootstrap.min.css';
function App() {
  return (
    <div className='App'>
      <Layout>
        <MainPage />
      </Layout>
    </div>
  );
}

export default App;
